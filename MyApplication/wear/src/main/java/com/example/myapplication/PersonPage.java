package com.example.myapplication;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Layout for Page
 *
 * @author Mike
 */
public class PersonPage extends LinearLayout {
    Context mContext;

    Button sleepButton, feedButton, cleanButton, walkButton;

    public static final int CALL_NUMBER = 1001;
    public PersonPage(Context context) {
        super(context);

        init(context);
    }

    public PersonPage(Context context, AttributeSet attrs) {
        super(context, attrs);

        init(context);
    }

    private void init(Context context) {
        mContext = context;

        // inflate XML layout
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.rect_activity_my, this, true);

        sleepButton = (Button) findViewById(R.id.sleep);
        feedButton = (Button) findViewById(R.id.feed_food);
        cleanButton = (Button) findViewById(R.id.clean);
        walkButton = (Button) findViewById(R.id.go_out);


        cleanButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                String callNumber = (String) cleanButton.getTag();

                Toast.makeText(mContext, "Selected: " + callNumber, Toast.LENGTH_LONG).show();

            }
        });

    }
/*
    public void setImage(int resId) {
        iconImage.setImageResource(resId);
    }
*/
    public void setSleepButton(String number) {
        sleepButton.setTag(number);
    }
    public void setFeedButton(String number) {
        feedButton.setTag(number);
    }
    public void setCleanButton(String number) {
        cleanButton.setTag(number);
    }
    public void setWalkButton(String number) {
        walkButton.setTag(number);
    }
/*
    public String getNameText() {
        return nameText.getText().toString();
    }

    public void setNameText(String nameStr) {
        nameText.setText(nameStr);
    }
*/
}

