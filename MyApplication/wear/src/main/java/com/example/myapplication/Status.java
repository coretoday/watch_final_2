package com.example.myapplication;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.Timer;
import java.util.TimerTask;

public class Status extends LinearLayout {
    Context mContext;
    private TimerTask second;
    private TextView timer_text;
    private final Handler handler = new Handler();
    public long timer_sec_1=0;
    public long timer_sec_2=0;
    public long timer_sec_3=0;
    public long timer_sec_4=0;

    private TimerTask tt1;
    private TimerTask tt2;
    private TimerTask tt3;
    private TimerTask tt4;

    private final Handler handler1 = new Handler();
    private final Handler handler2 = new Handler();
    private final Handler handler3 = new Handler();
    private final Handler handler4 = new Handler();

    ProgressBar hunger_bar, clean_bar, boring_bar, sleepy_bar;
    public static final int CALL_NUMBER = 1001;

    public Status(Context context){
        super(context);

        init(context);
    }

    public Status(Context context, AttributeSet attrs) {
        super(context, attrs);

        init(context);
    }

    private void init(Context context) {
        mContext = context;

        // inflate XML layout
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.status, this, true);


        hunger_bar = (ProgressBar) findViewById(R.id.hunger_progress);
        clean_bar = (ProgressBar) findViewById(R.id.clean_progress);
        boring_bar = (ProgressBar) findViewById(R.id.boring_progress);
        sleepy_bar = (ProgressBar) findViewById(R.id.sleepy_progress);
        testStart_1();
        testStart_2();
        testStart_3();
        testStart_4();

    }

    public void testStart_1() {


        tt1 = new TimerTask() {

            @Override
            public void run() {
                Log.i("Test", "Timer start");
                Update_1();
                timer_sec_1++;
            }
        };
        Timer timer = new Timer();
        timer.schedule(tt1, 0, 4000);
    }
    protected void Update_1() {
        Runnable updater = new Runnable() {

            public void run() {
                if(timer_sec_1<=0){timer_sec_1=100;}
                else{hunger_bar.setProgress(100-(int)timer_sec_1);}
            }

        };        handler1.post(updater);
    }

    public void testStart_2() {

        tt2 = new TimerTask() {

            @Override
            public void run() {
                Log.i("Test", "Timer start");
                Update_2();
                timer_sec_2++;
            }
        };
        Timer timer = new Timer();
        timer.schedule(tt2, 0, 8000);
    }
    protected void Update_2() {
        Runnable updater = new Runnable() {

            public void run() {
                if(timer_sec_2<=0){timer_sec_2=100;}
                else{clean_bar.setProgress(100-(int)timer_sec_2);}
            }

        };        handler2.post(updater);
    }


    public void testStart_3() {


        tt3 = new TimerTask() {

            @Override
            public void run() {
                Log.i("Test", "Timer start");
                Update_3();
                timer_sec_3++;
            }
        };
        Timer timer = new Timer();
        timer.schedule(tt3, 0, 12000);
    }
    protected void Update_3() {
        Runnable updater = new Runnable() {

            public void run() {
                if(timer_sec_3<=0){timer_sec_3=100;}
                else{boring_bar.setProgress(100-(int)timer_sec_3);}
            }

        };        handler3.post(updater);
    }



    public void testStart_4() {


        tt4 = new TimerTask() {

            @Override
            public void run() {
                Log.i("Test", "Timer start");
                Update_4();
                timer_sec_4++;
            }
        };
        Timer timer = new Timer();
        timer.schedule(tt4, 0, 16000);
    }
    protected void Update_4() {
        Runnable updater = new Runnable() {

            public void run() {
                if(timer_sec_4<=0){timer_sec_4=100;}
                else{sleepy_bar.setProgress(100-(int)timer_sec_4);}
            }

        };        handler4.post(updater);
    }



}

