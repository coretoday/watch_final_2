package com.example.myapplication;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import static android.provider.BaseColumns._ID;
import static com.example.myapplication.Constants.INDEX4;
import static com.example.myapplication.Constants.INDEX3;
import static com.example.myapplication.Constants.INDEX1;
import static com.example.myapplication.Constants.INDEX2;
import static com.example.myapplication.Constants.TABLE_NAME;
import static com.example.myapplication.Constants.TIMER1;
import static com.example.myapplication.Constants.TIMER2;

public class StorageOfStatus extends SQLiteOpenHelper {




	private static final String DATABASE_NAME = "status_database.db";
	private static final int DATABASE_VERSION =2;




	public StorageOfStatus(Context context){
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}
	
	
	
	@Override
	
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		db.execSQL("CREATE TABLE " + TABLE_NAME + " ("+_ID+" INTEGER PRIMARY KEY AUTOINCREMENT, " +
     TIMER1 +" LONG);");
        //, "+INDEX1+" INTEGER, " + INDEX2+ " INTEGER, " + INDEX3 + "INTEGER, "+ INDEX4 + "INTEGER );");
}

	
	

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
        db.execSQL("DROP TABLE IF EXISTS "+ TABLE_NAME);
		onCreate(db);
	}

}