package com.example.myapplication;

import android.provider.BaseColumns;

public interface Constants extends BaseColumns {
    public static final String INDEX4 = "BORE";
	public static final String INDEX3 = "SLEEP";
	public static final String INDEX2 = "CLEAN";
	public static final String INDEX1 = "HUNGER";
    public static final String TABLE_NAME = "STATUS";
    public static final String TIMER1 = "CHAR_IMAGE_TIME";
    public static final String TIMER2 = "STATUS_TIME";
    public static final String Date = "Date";
}