package com.example.myapplication;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.Timer;
import java.util.TimerTask;

public class MainPage extends LinearLayout {
    Context mContext;
    private TimerTask second;
    private ImageView black, white;
    private final Handler handler = new Handler();
    public int timer_sec =0;

    public static final int CALL_NUMBER = 1001;

    public MainPage(Context context) {
        super(context);

        init(context);
    }

    public MainPage(Context context, AttributeSet attrs) {
        super(context, attrs);

        init(context);
    }

    private void init(Context context) {
        mContext = context;

        // inflate XML layout
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.mainpage, this, true);
       testStart();

    }
    public void testStart() {
        black = (ImageView) findViewById(R.id.black);
        white = (ImageView) findViewById(R.id.white);
        second = new TimerTask() {


            @Override
            public void run() {
                Log.i("Test", "Timer start");
                if(timer_sec%2==0) Update();
                else Update2();
                timer_sec++;
            }
        };
        Timer timer = new Timer();
        timer.schedule(second, 0, 1000);
    }
    protected void Update() {
        Runnable updater = new Runnable() {
            public void run() {
                black.setVisibility(View.INVISIBLE);
            }
        };
        handler.post(updater);
    }

    protected void Update2() {
        Runnable updater = new Runnable() {
            public void run() {
                black.setVisibility(View.VISIBLE);
            }
        };
        handler.post(updater);
    }

}

